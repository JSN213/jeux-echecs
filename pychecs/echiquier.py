# -*- coding: utf-8 -*-

from pychecs.piece import Piece, Pion, Fou, Roi, Cavalier, Tour, Dame ,UTILISER_UNICODE


class Echiquier:
    """Classe Echiquier, implémentée avec un dictionnaire de pièces.


    Attributes:
        dictionnaire_pieces (dict): Un dictionnaire dont les clés sont des positions, suivant le format suivant:
            Une position est une chaîne de deux caractères.
            Le premier caractère est une lettre entre a et h, représentant la colonne de l'échiquier.
            Le second caractère est un chiffre entre 1 et 8, représentant la rangée de l'échiquier.
        chiffres_rangees (list): Une liste contenant, dans l'ordre, les chiffres représentant les rangées.
        lettres_colonnes (list): Une liste contenant, dans l'ordre, les lettres représentant les colonnes.

    """


    def __init__(self):
        # Le dictionnaire de pièces, vide au départ, mais ensuite rempli par la méthode initialiser_echiquier_depart().
        self.dictionnaire_pieces = {}


        # Ces listes pourront être utilisées dans les autres méthodes, par exemple pour valider une position.
        self.chiffres_rangees = ['1', '2', '3', '4', '5', '6', '7', '8']
        self.lettres_colonnes = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

        self.initialiser_echiquier_depart()

    def position_est_valide(self, position):
        """Vérifie si une position est valide (dans l'échiquier). Une position est une concaténation d'une lettre de
        colonne et d'un chiffre de rangée, par exemple 'a1' ou 'h8'.

        Args:
            position (str): La position à valider.

        Returns:
            bool: True si la position est valide, False autrement.

        """
        i = position[0]
        j = position[1]

        liste_rangees = self.chiffres_rangees
        liste_colonne = self.lettres_colonnes

        if i in liste_colonne and j in liste_rangees:  # Si le chiffre est dans la liste de rangées
            return True                                # et que la lettre est dans la liste de colonnes, vrai

        return False

    def recuperer_piece_a_position(self, position):
        """Retourne la pièce qui est située à une position particulière, reçue en argument. Si aucune pièce n'est
        située à cette position, retourne None.

        Args:
            position (str): La position où récupérer la pièce.

        Returns:
            Piece or None: Une instance de type Piece si une pièce était située à cet endroit, et None autrement.

        """
        try:
            liste_cles = self.dictionnaire_pieces.keys()

            if position in liste_cles:  # Si la position fait parti de la liste des clées du dictionnaire, on retourne la pièce à cet endroit
                return self.dictionnaire_pieces[position]
            return None
        except:
            return None

    def couleur_piece_a_position(self, position):
        """Retourne la couleur de la pièce située à la position reçue en argument, et une chaîne vide si aucune
        pièce n'est à cet endroit.

        Args:
            position (str): La position où récupérer la couleur de la pièce.

        Returns:
            str: La couleur de la pièce s'il y en a une, et '' autrement.

        """
        if self.recuperer_piece_a_position(position) != None:  # S'il y a une pièce à cette position, on retourne la couleur de cette pièce
            piece = self.dictionnaire_pieces[position]
            couleur = piece.couleur
            return couleur
        return ''

    def rangees_entre(self, rangee_debut, rangee_fin):
        """Retourne la liste des rangées qui sont situées entre les deux rangées reçues en argument, exclusivement.
        Attention de conserver le bon ordre.

        Args:
            rangee_debut (str): Le caractère représentant la rangée de début, par exemple '1'.
            rangee_fin (str): Le caractère représentant la rangée de fin, par exemple '4'.

        Exemple:
            >>> echiquer.rangees_entre('1', '1')
            []
            >>> echiquier.rangees_entre('2', '3')
            []
            >>> echiquier.rangees_entre('2', '8')
            ['3', '4', '5', '6', '7']
            >>> echiquier.rangees_entre('8', '3')
            ['7', '6', '5', '4']

        Indice:
            Utilisez self.chiffres_rangees pour obtenir une liste des rangées valides.

        Returns:
            list: Une liste des rangées (en str) entre le début et la fin, dans le bon ordre.

        """

        liste_rangees2 = self.chiffres_rangees

        if int(rangee_debut) == int(rangee_fin):  # Lorsqu'on reste dans la même rangée, la liste de rangées entre est vide
            return []

        elif int(rangee_debut) < int(rangee_fin):  # ici on traite le cas où la rangée de début est inférieure à la rangée de fin
            liste_entre_rangee = liste_rangees2[int(rangee_debut): int(rangee_fin)-1]  # Les rangées entre sont alors la liste de rangées sur l'intervalle écrit
            return liste_entre_rangee

        elif int(rangee_debut) > int(rangee_fin): # Le cas où la rangée de début est supérieure
            liste_entre_rangee = liste_rangees2[int(rangee_fin): int(rangee_debut)-1]  # on fait la même chose que plus haut avec un intervalle un peu différent
            liste_entre_rangee = liste_entre_rangee[::-1]  # On inverse la liste pour l'avoir dans l'ordre désiré
            return liste_entre_rangee

    def colonnes_entre(self, colonne_debut, colonne_fin):
        """Retourne la liste des colonnes qui sont situées entre les deux colonnes reçues en argument, exclusivement.
        Attention de conserver le bon ordre.

        Args:
            colonne_debut (str): Le caractère représentant la colonne de début, par exemple 'a'.
            colonne_fin (str): Le caractère représentant la colonne de fin, par exemple 'h'.

        Exemple:
            >>> echiquer.colonnes_entre('a', 'a')
            []
            >>> echiquier.colonnes_entre('b', 'c')
            []
            >>> echiquier.colonnes_entre('b', 'h')
            ['c', 'd', 'e', 'f', 'g']
            >>> echiquier.colonnes_entre('h', 'c')
            ['g', 'f', 'e', 'd']

        Indice:
            Utilisez self.lettres_colonnes pour obtenir une liste des colonnes valides.

        Returns:
            list: Une liste des colonnes (en str) entre le début et la fin, dans le bon ordre.

        """
        liste_colonne2 = self.lettres_colonnes

        for i in range(len(liste_colonne2)):
            if liste_colonne2[i] == colonne_debut:  # si la condition est respectée, i est la position de début dans la liste
                for j in range(len(liste_colonne2)):
                    if liste_colonne2[j] == colonne_fin:  # si la condition est respectée, j est la position de fin dans la liste
                        if i < j:  # si la position de début est inférieure à la position de fin
                            return liste_colonne2[i+1:j]  # on retourne la liste de colonnes sur cet intervalle
                        elif i == j:  # s'il sont égal, il n'y a pas de colonnes entre
                            return []
                        elif i > j:  # si la position de début est supérieure à la position de fin
                            liste_colonne3 = liste_colonne2[j+1:i]  # on fait comme plus haut, mais avec un petit changement d'intervalle
                            if i - j > 2:  # si la différence entre les positions est plus grande que 2, on inverse la liste pour l'avoir dans l'ordre désiré
                                liste_bon_ordre = liste_colonne3[:: -1]
                                return liste_bon_ordre
                            else:  # aussi non on retourne la liste sans l'inversée (1 seul élément ou 0)
                                return liste_colonne3

    def chemin_libre_entre_positions(self, position_source, position_cible):
        """Vérifie si la voie est libre entre deux positions, reçues en argument. Cette méthode sera pratique
        pour valider les déplacements: la plupart des pièces ne peuvent pas "sauter" par dessus d'autres pièces,
        il faut donc s'assurer qu'il n'y a pas de pièce dans le chemin.

        On distingue quatre possibilités (à déterminer dans votre code): Soit les deux positions sont sur la même
        rangée, soit elles sont sur la même colonne, soit il s'agit d'une diagonale, soit nous sommes dans une
        situation où nous ne pouvons pas chercher les positions "entre" les positions source et cible. Dans les trois
        premiers cas, on fait la vérification et on retourne True ou False dépendamment la présence d'une pièce ou non.
        Dans la dernière situation, on considère que les positions reçues sont invalides et on retourne toujours False.

        Args:
            position_source (str): La position source.
            position_cible (str): La position cible.

        Warning:
            Il ne faut pas vérifier les positions source et cible, puisqu'il peut y avoir des pièces à cet endroit.
            Par exemple, si une tour "mange" un pion ennemi, il y aura une tour sur la position source et un pion
            sur la position cible.

        Returns:
            bool: True si aucune pièce n'est située entre les deux positions, et False autrement (ou si les positions
                ne permettaient pas la vérification).

        """
        liste_rangees4 = self.rangees_entre(position_source[1], position_cible[1])  # une liste des rangées entre les positions en entrée
        liste_colonne4 = self.colonnes_entre(position_source[0], position_cible[0])  # une liste des colonnes entre les positions en entrée

        if position_source[1] == position_cible[1]:  # as 1, lorsqu'ils sont sur la même rangée
            for colonne in liste_colonne4:
                position_a_verifier = colonne + position_source[1]  # on crée une position à vérifier avec tous les colonnes entre et la rangée
                if self.recuperer_piece_a_position(position_a_verifier) != None:  # s'il y a une pièce sur une des positions à vérifier
                    return False                                                  # le chemin n'est pas libre
            return True

        elif position_source[0] == position_cible[0]:  # cas 2, lorsqu'ils sont sur la même colonne c'est le même principe que pour les rangées
            for i in liste_rangees4:
                position_a_verifier = position_source[0]+i
                if self.recuperer_piece_a_position(position_a_verifier) != None:
                    return False
            return True

        # cas3, diagonales
        elif position_cible[0] != position_source[0] and position_cible[1] != position_source[1] and len(liste_rangees4) == len(liste_colonne4):
            for i in range(len(liste_rangees4)):                           # on place les éléments des deux listes (les colonnes entre et les rangées entre)
                position_a_verifier = liste_colonne4[i]+liste_rangees4[i]  # ensemble 2 à 2 dans l'ordre pour obtenir les positions à vérifier
                if self.recuperer_piece_a_position(position_a_verifier) != None:  # même principe que pour le cas 1 et 2
                        return False
            return True

        else:
            return False  # cas 4

    def deplacement_est_valide(self, position_source, position_cible):
        """Vérifie si un déplacement serait valide dans l'échiquier actuel. Notez que chaque type de
        pièce se déplace de manière différente, vous voudrez probablement utiliser le polymorphisme :-).

        Règles pour qu'un déplacement soit valide:
            1. Il doit y avoir une pièce à la position source.
            2. La position cible doit être valide (dans l'échiquier).
            3. Si la pièce ne peut pas sauter, le chemin doit être libre entre les deux positions.
            4. S'il y a une pièce à la position cible, elle doit être de couleur différente.
            5. Le déplacement doit être valide pour cette pièce particulière.

        Args:
            position_source (str): La position source du déplacement.
            position_cible (str): La position cible du déplacement.

        Returns:
            bool: True si le déplacement est valide, et False autrement.

        """


        piece1 = self.recuperer_piece_a_position(position_source)
        piece2 =  self.recuperer_piece_a_position(position_cible)

        try:
            if piece1 != None:  # s'il y a une pièce sur la case de départ
                if self.position_est_valide(position_cible):  #si la position cible est valide
                    if self.chemin_libre_entre_positions(position_source, position_cible) or type(piece1) == Cavalier:  # si le chemin est libre entre les positions ou que la pièce1 est un cavalier
                        if piece1.peut_se_deplacer_vers(position_source, position_cible):  # si la piece1 peut se déplacer vers la position cible
                            if piece2 == None:                                             # et qu'il n'y a pas de pièce sur la case cible
                                return True                                                # alors le déplacement est valide
                        if piece1.peut_faire_une_prise_vers(position_source, position_cible):  # si la piece1 peut faire une prise vers la position cible
                            if piece2 != None:                                                 # et qu'il y a une pièce sur la position cible
                                if piece1.couleur != piece2.couleur:                           # et que la couleur des deux pièces est différente
                                    return True                                                # alors le déplacement est valide
                                elif piece1.couleur == piece2.couleur:                         # si les deux pièces ont la même couleur
                                    return False                                               # le déplacement est invalide
            return False
        except:
            return False

    def deplacer(self, position_source, position_cible):
        """Effectue le déplacement d'une pièce en position source, vers la case en position cible. Vérifie d'abord
        si le déplacement est valide, et ne fait rien (puis retourne False) dans ce cas. Si le déplacement est valide,
        il est effectué (dans l'échiquier actuel) et la valeur True est retournée.

        Args:
            position_source (str): La position source.
            position_cible (str): La position cible.

        Returns:
            bool: True si le déplacement était valide et a été effectué, et False autrement.

        """
        try:
            if self.deplacement_est_valide(position_source, position_cible):  # Si le déplacement est valide
                a = self.dictionnaire_pieces                                  # on ajoute la pièce de la position source dans le dictionnaire, mais avec
                a[position_cible] = a[position_source]                        # la position cible comme clée
                del a[position_source]                                        # on efface la position source du dictionnaire

                return True
            return False
        except:
            return False

    def roi_de_couleur_est_dans_echiquier(self, couleur):
        """Vérifie si un roi de la couleur reçue en argument est présent dans l'échiquier.

        Args:
            couleur (str): La couleur (blanc ou noir) du roi à rechercher.

        Returns:
            bool: True si un roi de cette couleur est dans l'échiquier, et False autrement.

        """
        for positions in self.dictionnaire_pieces.keys():  # on parcourt tous les positions avec des pièces
            a = self.recuperer_piece_a_position(positions)
            if type(a) == Roi and a.couleur == couleur:  # on vérifie si la pièce à cette position est de type Roi et de la couleur demandée
                return True

        return False

    def initialiser_echiquier_depart(self):
        """Initialise l'échiquier à son contenu initial. Pour faire vos tests pendant le développement,
        nous vous suggérons de vous fabriquer un échiquier plus simple, en modifiant l'attribut
        dictionnaire_pieces de votre instance d'Echiquier.

        """
        self.dictionnaire_pieces = {
            'a1': Tour('B'), 'b1': Cavalier('B'), 'c1': Fou('B'), 'd1': Dame('B'), 'e1': Roi('B'), 'f1':Fou('B'), 'g1':Cavalier('B'), 'h1':Tour('B'),
                       'a2':Pion('B'), 'b2':Pion('B'), 'c2':Pion('B'), 'd2':Pion('B'), 'e2':Pion('B'), 'f2':Pion('B'), 'g2':Pion('B'), 'h2': Pion('B'),
                       'a7':Pion('N'), 'b7':Pion('N'), 'c7':Pion('N'), 'd7':Pion('N'), 'e7':Pion('N'), 'f7':Pion('N'), 'g7':Pion('N'), 'h7':Pion('N'),
                       'a8':Tour('N'), 'b8':Cavalier('N'), 'c8':Fou('N'), 'd8':Dame('N'), 'e8': Roi('N'), 'f8':Fou('N'), 'g8': Cavalier('N'), 'h8':Tour('N')
        }
    def dictionnaire_keys(self):
        mykeys = self.dictionnaire_pieces

if __name__ == '__main__':
    e = Echiquier()
    e.initialiser_echiquier_depart()
    print(e.recuperer_piece_a_position('a1'))