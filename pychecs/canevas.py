from tkinter import *
import ast
from tkinter.filedialog import *
from tkinter.messagebox import *

from pychecs.echiquier  import *


class CanevasEchiquier(Canvas):
    """
    Classe qui hérite de Canevas du module Tkinter.
    """

    def __init__(self, parent, n_pixels_par_case):

        self.echiquier = Echiquier()

        # Nombre de lignes et de colonnes
        self.n_lignes = 8
        self.n_colonnes = 8

        self.list_index = []
        self.list_pieces = []

        self.echiquier.initialiser_echiquier_depart()

        self.n_pixels_par_case = n_pixels_par_case

        self.couleur1 = 'white'
        self.couleur2 = 'grey'

        self.caracteres_pieces = {}

        self.chiffres_des_rangees = ['1', '2', '3', '4', '5', '6', '7', '8']
        self.lettres_des_colonnes = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

        # Pièces et positions
        self.pieces = self.echiquier.dictionnaire_pieces

        super().__init__(parent, width=self.n_lignes*n_pixels_par_case, height=self.n_colonnes*n_pixels_par_case)

        #redimensionner si nécessaire
        self.bind('<Configure>', self.redimensionner)

    def joueur_suivant(self):
        """Change le joueur actif: passe de blanc à noir, ou de noir à blanc, selon la couleur du joueur actif.
        """

        while 1: # On met une boucle pour pouvoir terminer la fonction dès qu'une condition est atteinte
            if self.joueur_actif == 'blanc':
                self.joueur_actif = 'noir'
                break
            elif self.joueur_actif == 'noir':
                self.joueur_actif = 'blanc'
                break

    def dessiner_cases(self):
        """
        Dessin des cases de l'échiquier
        """

        for i in range(self.n_lignes):
            for j in range(self.n_colonnes):
                debut_ligne = i * self.n_pixels_par_case
                fin_ligne = debut_ligne + self.n_pixels_par_case
                debut_colonne = j * self.n_pixels_par_case
                fin_colonne = debut_colonne + self.n_pixels_par_case

                #on alterne les couleurs
                if (i+j) % 2 == 0:
                    couleur = self.couleur1

                else:
                    couleur = self.couleur2

                indexer = '{},{}'.format(i, j)
                self.list_index += [indexer]

                self.create_rectangle(debut_colonne, debut_ligne, fin_colonne, fin_ligne, fill='cyan', disabledfill=couleur, tags=indexer, state=DISABLED)

    def dessiner_pieces(self):
        """
        Dessiner les pieces de l'échiquier
        """
        self.caracteres_pieces = {Pion('B'): '\u2659',
                             Pion('N'): '\u265f',
                             Tour('B'): '\u2656',
                             Tour('N'): '\u265c',
                             Cavalier('B'): '\u2658',
                             Cavalier('N'): '\u265e',
                             Fou('B'): '\u2657',
                             Fou('N'): '\u265d',
                             Roi('B'): '\u2654',
                             Roi('N'): '\u265a',
                             Dame('B'): '\u2655',
                             Dame('N'): '\u265b'
                            }

        for position, piece in self.pieces.items():

            coordonnee_y = (self.n_lignes - self.chiffres_des_rangees.index(position[1])-1)*self.n_pixels_par_case+self.n_pixels_par_case//2
            coordonnee_x = self.lettres_des_colonnes.index(position[0])*self.n_pixels_par_case + self.n_pixels_par_case // 2
            indexer = '{},{}'.format(piece, position)
            self.list_pieces = [indexer]

            self.create_text(coordonnee_x, coordonnee_y, text=piece, tags=indexer,
                             font=('DejaVu', self.n_pixels_par_case//2))

    def redimensionner(self, event):
        nouvelle_taille = min(event.width, event.height)

        self.n_pixels_par_case = nouvelle_taille//self.n_lignes

        for tag in self.list_index:
            self.delete(str(tag))
        self.dessiner_cases()

        for position, piece in self.pieces.items():
            indexer = '{},{}'.format(piece, position)
            self.list_pieces = [indexer]
            self.delete(indexer)
        self.dessiner_pieces()

class Fenetre(Tk):

    def __init__(self):
        super().__init__()
        self.echec = Echiquier()
        self.nom_fichier = ''

        self.drag_object = str()

        self.positionSource = str()

        self.list_movs = []
        self.joueur_actif = 'B'
        self.positionsource = ''
        self.positioncible = str()

        self._drag_data = {"x": 0, "y": 0}

        self.canevas_echiquier = CanevasEchiquier(self, 70)
        self.canevas_echiquier.grid(sticky=NSEW)
        self.tag = ''

        #titre de la fenetre
        self.title("Échiquier")

        #clic souris
        self.canevas_echiquier.bind('<Button-1>', self.selectionner_ou_deplacer)

        #Bouton de sauvegarde
        self.sauvegarder = Button(self)
        self.sauvegarder.grid()
        self.sauvegarder['text'] = 'Sauvegarder'
        self.sauvegarder['command'] = self.sauvegarder_partie

        #Bouton de téléchargement
        self.telecharger = Button(self)
        self.telecharger.grid()
        self.telecharger['text'] = 'Télécharger'
        self.telecharger['command'] = self.telecharger_partie

        #Bouton de nouvelle partie
        self.nouvelle_partie = Button(self)
        self.nouvelle_partie.grid()
        self.nouvelle_partie['text'] = 'Nouvelle partie'
        self.nouvelle_partie['command'] = self.partie

        #Bouton pour changer de thème
        self.theme = Button(self)
        self.theme.grid()
        self.theme['text'] = 'Changer le thème'
        self.theme['command'] = self.changer_theme

        #Label qui servira à indiquer la pièce récupérée sur la case sélectionnée
        self.messages = Label(self)
        self.messages.grid()
        self.messages['background'] = 'yellow'

        #Label qui indique le joueur actif
        self.messages2 = Label(self)
        self.messages2.grid(columnspan = 1)
        self.messages2['background'] = 'purple'
        self.messages2['text'] = "Joueur actif: {}".format(self.joueur_actif)
        self.piece_selectionnee = ''
        self.position_selectionnee = ''

        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.protocol("WM_DELETE_WINDOW", self.on_exit)

    def on_exit(self):
        # Quand on clique  pour quitter une fenêtre nous demande si on veux sauvegarder avant
        if askyesno("Sauvegarder", "Voulez-vous sauvegarder?"):
            self.sauvegarder_partie()
        self.destroy()

    #fonction pour une nouvelle partie
    def partie(self):
        #Si c'est une nouvelle partie car la précédente était terminée on ne demande pas de sauvegarder
        if self.echec.roi_de_couleur_est_dans_echiquier(self.joueur_actif):
            if askyesno("Sauvegarder", "Voulez-vous sauvegarder?"):
                self.sauvegarder_partie()
            self.joueur_actif = 'B'
            self.messages2['text'] = "Joueur actif: {}".format(self.joueur_actif)
            self.canevas_echiquier.pieces = self.echec.dictionnaire_pieces
            for position, piece in self.canevas_echiquier.pieces.items():

                    indexer = '{},{}'.format(piece, position)
                    self.canevas_echiquier.list_pieces = [indexer]
                    self.canevas_echiquier.delete(indexer)

            self.echec.initialiser_echiquier_depart()
            self.canevas_echiquier.pieces = self.echec.dictionnaire_pieces
            self.canevas_echiquier.dessiner_pieces()

        #Si c'est une nouvelle partie car on a cliquer sur le bouton nouvelle partie, on demande de sauvegarder
        elif not self.echec.roi_de_couleur_est_dans_echiquier(self.joueur_actif):
            self.joueur_actif = 'B'
            self.messages2['text'] = "Joueur actif: {}".format(self.joueur_actif)
            self.canevas_echiquier.pieces = self.echec.dictionnaire_pieces
            for position, piece in self.canevas_echiquier.pieces.items():

                    indexer = '{},{}'.format(piece, position)
                    self.canevas_echiquier.list_pieces = [indexer]
                    self.canevas_echiquier.delete(indexer)

            self.echec.initialiser_echiquier_depart()
            self.canevas_echiquier.pieces = self.echec.dictionnaire_pieces
            self.canevas_echiquier.dessiner_pieces()

    #fonction pour changer le thème
    def changer_theme(self):
        if self.canevas_echiquier.couleur2 == 'grey':
            self.canevas_echiquier.couleur2 = 'red'
        elif self.canevas_echiquier.couleur2 == 'red':
            self.canevas_echiquier.couleur2 = 'grey'
        self.canevas_echiquier.dessiner_cases()
        self.canevas_echiquier.dessiner_pieces()

    #fonction pour enregistrer le dictionnaire de pièces comme désiré pour être réutilisé plus tard
    def sauvegarder_partie(self):
        dict_piece = {'\u2659': 'PB',
                             '\u265f': 'PN',
                             '\u2656': 'TB',
                             '\u265c': 'TN',
                             '\u2658': 'CB',
                             '\u265e': 'CN',
                             '\u2657': 'FB',
                             '\u265d': 'FN',
                             '\u2654': 'RB',
                             '\u265a': 'RN',
                             '\u2655': 'DB',
                             '\u265b': 'DN'
                            }

        filename = asksaveasfilename(title="Sauvegarder votre document", filetypes=[('txt files', '.txt'), ('all files', '.*')])
        fichier = open(filename, "w", encoding='utf8')
        for caractere in dict_piece.keys():
            for cle in self.canevas_echiquier.caracteres_pieces.keys():
                value = self.canevas_echiquier.caracteres_pieces.get(cle)
                if value == caractere:
                    self.canevas_echiquier.caracteres_pieces[cle] = dict_piece.get(caractere)

        dicto_debase = self.echec.dictionnaire_pieces.copy()
        for caractere in self.canevas_echiquier.caracteres_pieces.keys():
            for cle in dicto_debase.keys():
                value = dicto_debase.get(cle)
                if type(value) == type(caractere) and value.couleur == caractere.couleur:
                    dicto_debase[cle] = self.canevas_echiquier.caracteres_pieces.get(caractere)

        contenu = dicto_debase

        dict_piece2 = {'PB': Pion('B'),
                             'PN': Pion('N'),
                             'TB': Tour('B'),
                             'TN': Tour('N'),
                             'CB': Cavalier('B'),
                             'CN': Cavalier('N'),
                             'FB': Fou('B'),
                             'FN': Fou('N'),
                             'RB': Roi('B'),
                             'RN': Roi('N'),
                             'DB': Dame('B'),
                             'DN': Dame('N')
                            }
        for caractere in dict_piece2.keys():
            for cle in self.canevas_echiquier.caracteres_pieces.keys():
                value = self.canevas_echiquier.caracteres_pieces.get(cle)
                if value == caractere:
                    self.canevas_echiquier.caracteres_pieces[cle] = dict_piece2.get(caractere)

        couleur_joueur = self.joueur_actif

        fichier.write('{}{}'.format(str(contenu), couleur_joueur))
        fichier.close()

    #fonction pour transformer le document enregistré en partie
    def telecharger_partie(self):
        if askyesno("Sauvegarder", "Voulez-vous sauvegarder?"):
            self.sauvegarder_partie()

        filename = askopenfilename(title="Ouvrir votre document", filetypes=[('txt files', '.txt'), ('all files', '.*')])
        fichier = open(filename, "r", encoding='utf8')
        contenu = fichier.read()

        self.joueur_actif = contenu[-1]
        contenu = contenu[:-1]

        self.messages2['text'] = "Joueur actif: {}".format(self.joueur_actif)

        dicto_sauver = ast.literal_eval(contenu)

        fichier.close()
        dict_piece2 = {'PB': Pion('B'),
                             'PN': Pion('N'),
                             'TB': Tour('B'),
                             'TN': Tour('N'),
                             'CB': Cavalier('B'),
                             'CN': Cavalier('N'),
                             'FB': Fou('B'),
                             'FN': Fou('N'),
                             'RB': Roi('B'),
                             'RN': Roi('N'),
                             'DB': Dame('B'),
                             'DN': Dame('N')
                            }

        for caractere in dict_piece2.keys():
            for cle in dicto_sauver.keys():
                value = dicto_sauver.get(cle)
                if value == caractere:
                    dicto_sauver[cle] = dict_piece2[caractere]
        del (self.echec.dictionnaire_pieces)
        self.echec.dictionnaire_pieces = dicto_sauver.copy()

        self.canevas_echiquier.pieces = self.echec.dictionnaire_pieces

        for position, piece in self.canevas_echiquier.pieces.items():

            indexer = '{},{}'.format(piece, position)
            self.canevas_echiquier.list_pieces = [indexer]
            self.canevas_echiquier.delete(indexer)
        self.canevas_echiquier.dessiner_cases()
        self.canevas_echiquier.dessiner_pieces()

    def selectionner_ou_deplacer(self, event):
         ligne_case = event.y//self.canevas_echiquier.n_pixels_par_case
         colonne_case = event.x//self.canevas_echiquier.n_pixels_par_case
         position = "{}{}".format(self.canevas_echiquier.lettres_des_colonnes[colonne_case],
                                  self.canevas_echiquier.chiffres_des_rangees[self.canevas_echiquier.n_lignes-ligne_case-1])

         for movs in self.list_movs:
             pos_mov = "{}{}".format(self.canevas_echiquier.lettres_des_colonnes[int(movs[2])],
                        self.canevas_echiquier.chiffres_des_rangees[self.canevas_echiquier.n_lignes-(int(movs[0]))-1])

             if position == pos_mov:
                 self.drop(event)

         self.selectionner(event)

    def selectionner(self, event):
        #déterminer la case où on clique
        ligne_case = event.y//self.canevas_echiquier.n_pixels_par_case
        colonne_case = event.x//self.canevas_echiquier.n_pixels_par_case
        position = "{}{}".format(self.canevas_echiquier.lettres_des_colonnes[colonne_case],
                                 self.canevas_echiquier.chiffres_des_rangees[self.canevas_echiquier.n_lignes-ligne_case-1])
        self.position_selectionnee = position

        self.couleur_case_change(event)
        self.aff_prise_deplacement(event)
        self.drag(event)
        try:
            piece = self.echec.recuperer_piece_a_position(self.position_selectionnee)
            self.piece_selectionnee = piece

            self.messages['foreground'] = 'black'
            self.messages['text'] = "pièce sélectionée : {} à la position {}".format(piece, self.position_selectionnee)
            return position

        except:
            self.messages['text'] = "Erreur, aucune pièce à cet endroit"
            return position

    def couleur_case_change(self, event):
            ligne_case = event.y//self.canevas_echiquier.n_pixels_par_case
            colonne_case = event.x//self.canevas_echiquier.n_pixels_par_case

            if self.tag != '':
                self.canevas_echiquier.itemconfigure(self.tag, state=DISABLED)
                self.tag = ''
            self.tag = '{},{}'.format(ligne_case, colonne_case)

            self.canevas_echiquier.itemconfigure(self.tag, state=NORMAL)


    def aff_prise_deplacement(self,event):
        #Initialisation des cases colorées
        if len(self.list_movs) != 0:
            for mov in self.list_movs:
                self.canevas_echiquier.itemconfigure(mov, state=DISABLED)
        while len(self.list_movs) != 0:
                del(self.list_movs[0])
        #Création de la coordonnée de la position source
        ligne_case = event.y//self.canevas_echiquier.n_pixels_par_case
        colonne_case = event.x//self.canevas_echiquier.n_pixels_par_case

        positionS = "{}{}".format(self.canevas_echiquier.lettres_des_colonnes[colonne_case],
                    self.canevas_echiquier.chiffres_des_rangees[self.canevas_echiquier.n_lignes-ligne_case-1])
        piece2 = self.echec.recuperer_piece_a_position(positionS)

        sgat = self.canevas_echiquier.list_index

        for gat in sgat:

            positionC = "{}{}".format(self.canevas_echiquier.lettres_des_colonnes[int(gat[2])],
                        self.canevas_echiquier.chiffres_des_rangees[self.canevas_echiquier.n_lignes-(int(gat[0]))-1])

            if self.echec.deplacement_est_valide(positionS, positionC):
                self.list_movs += [gat]

        for mov in self.list_movs:
            self.canevas_echiquier.itemconfigure(mov, state=NORMAL)

    def drag(self, event):
        ligne_case = event.y//self.canevas_echiquier.n_pixels_par_case
        colonne_case = event.x//self.canevas_echiquier.n_pixels_par_case

        self.positionSource = "{}{}".format(self.canevas_echiquier.lettres_des_colonnes[colonne_case],
                              self.canevas_echiquier.chiffres_des_rangees[self.canevas_echiquier.n_lignes-ligne_case-1])

        self.drag_object = "{}{}".format(self.echec.recuperer_piece_a_position(self.positionSource), self.positionSource)
        self._drag_data["x"] = event.x
        self._drag_data["y"] = event.y
        self.canevas_echiquier.bind('<B1-Motion>', self.n)
        self.canevas_echiquier.bind('<ButtonRelease-1>', self.drop)

    def n(self, event):

        delta_x = event.x - self._drag_data["x"]
        delta_y = event.y - self._drag_data["y"]

        self.canevas_echiquier.itemconfigure(self.drag_object, coordonnee_x=delta_x, coordonnee_y=delta_y)
        #Attribuer la nouvelle position
        self._drag_data["x"] = event.x
        self._drag_data["y"] = event.y

    def drop(self, event):

            ligne_case = event.y//self.canevas_echiquier.n_pixels_par_case
            colonne_case = event.x//self.canevas_echiquier.n_pixels_par_case
            positionCible = "{}{}".format(self.canevas_echiquier.lettres_des_colonnes[colonne_case],
                            self.canevas_echiquier.chiffres_des_rangees[self.canevas_echiquier.n_lignes-ligne_case-1])
            piecejouer = self.echec.recuperer_piece_a_position(self.positionSource)

            try:
                if self.joueur_actif == piecejouer.couleur and self.positionSource != positionCible:
                    self.echec.deplacer(self.positionSource, positionCible)

                    self.canevas_echiquier.pieces = self.echec.dictionnaire_pieces
                    for position, piece in self.canevas_echiquier.pieces.items():

                        indexer = '{},{}'.format(piece, position)
                        self.canevas_echiquier.list_pieces = [indexer]
                        self.canevas_echiquier.delete(indexer)
                    self.canevas_echiquier.dessiner_cases()
                    self.canevas_echiquier.dessiner_pieces()

                    self.joueur_suivant()
                    self.promotion_pion()

                    dummy = 1
                    if not self.echec.roi_de_couleur_est_dans_echiquier(self.joueur_actif):
                         if askyesno("Sauvegarder", "Voulez vous jouer une autre partie?"):
                            self.partie()
                            dummy = 0

                         elif dummy:
                            self.quit()

                self._drag_data["x"] = 0
                self._drag_data["y"] = 0

            except:
                 if self.joueur_actif == None and self.positionSource != positionCible:
                    self.echec.deplacer(self.positionSource, positionCible)

                    self.canevas_echiquier.pieces = self.echec.dictionnaire_pieces
                    for position, piece in self.canevas_echiquier.pieces.items():

                        indexer = '{},{}'.format(piece, position)
                        self.canevas_echiquier.list_pieces = [indexer]
                        self.canevas_echiquier.delete(indexer)
                    self.canevas_echiquier.dessiner_cases()
                    self.canevas_echiquier.dessiner_pieces()

                    self.joueur_suivant()
                    self.promotion_pion()

                    dummy = 1
                    if not self.echec.roi_de_couleur_est_dans_echiquier(self.joueur_actif):
                         if askyesno("Sauvegarder", "Voulez vous jouer une autre partie?"):
                            self.partie()
                            dummy = 0

                         elif dummy:
                            self.quit()

                 self._drag_data["x"] = 0
                 self._drag_data["y"] = 0

    def joueur_suivant(self):
        """Change le joueur actif: passe de blanc à noir, ou de noir à blanc, selon la couleur du joueur actif.
        """
        while 1: # On met une boucle pour pouvoir terminer la fonction dès qu'une condition est atteinte
            if self.joueur_actif == 'B':
                self.joueur_actif = 'N'
                self.messages2['text'] = "Joueur actif: {}".format(self.joueur_actif)
                break
            elif self.joueur_actif == 'N':
                self.joueur_actif = 'B'
                self.messages2['text'] = "Joueur actif: {}".format(self.joueur_actif)
                break

    # Pas fonctionnelle, mais idée présente
    def promotion_pion(self):
        colonne = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        for position in self.echec.dictionnaire_pieces.keys():
            for lettre in colonne:
                piece = self.echec.dictionnaire_pieces[position]
                if position == lettre+'8' and type(piece) == type(Pion) and piece.couleur == 'B':
                    self.echec.dictionnaire_pieces[position] = Dame('B')
                    print(self.echec.dictionnaire_pieces[position])
        for position in self.echec.dictionnaire_pieces.keys():
            for lettre in colonne:
                if position == lettre+'1' and self.echec.dictionnaire_pieces[position] == Pion('N'):
                    self.echec.dictionnaire_pieces[position] = Dame('N')
        self.canevas_echiquier.pieces = self.echec.dictionnaire_pieces
        self.canevas_echiquier.dessiner_pieces()

    # Pas fonctionnelle, mais idée présente
    def roque(self, position_source, position_cible):
        if self.recuperer_piece_a_position(position_source) == Roi('B') or self.recuperer_piece_a_position(position_source) == Roi('N') and self.recuperer_piece_a_position(position_cible) == Tour('B') or self.recuperer_piece_a_position(position_cible) == Tour('N'):
            if self.recuperer_piece_a_position(position_source) == Tour('B') or self.recuperer_piece_a_position(position_source) == Tour('N') and self.recuperer_piece_a_position(position_cible) == Roi('B') or self.recuperer_piece_a_position(position_cible) == Roi('N'):
                piece = self.recuperer_piece_a_position(position_source)
                piece2 = self.recuperer_piece_a_position(position_cible)
                if piece.couleur == piece2.couleur and self.chemin_libre_entre_positions(position_source, position_cible):
                    if piece.couleur == 'B' and position_source[1] == 1 and position_cible[1] == 1:
                        return True
                    elif piece.couleur == 'N' and position_source[1] == 8 and position_cible[1] == 8:
                        return True
        return False

if __name__ == '__main__':
    # Création d'une instance de Partie
    fenetre = Fenetre()
    fenetre.mainloop()