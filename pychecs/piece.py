# -*- coding: utf-8 -*-
"""Module contenant la classe de base Piece, ainsi qu'une classe fille pour chacun des types de pièces du jeu d'échecs.

"""
# TODO: Si votre système n'affiche pas correctement les caractères unicodes du jeu d'échecs,
# mettez cette constante (variable globale) à False. Un tutoriel est présent sur le site Web
# du cours pour vous aider à faire fonctionner les caractères Unicoe sous Windows.
UTILISER_UNICODE = True


class Piece:
    """Une classe de base représentant une pièce du jeu d'échecs. C'est cette classe qui est héritée plus bas pour fournir
    une classe par type de pièce (Pion, Tour, etc.).

    Attributes:
        couleur (str): La couleur de la pièce, soit 'blanc' ou 'noir'.
        peut_sauter (bool): Si oui ou non la pièce peut "sauter" par dessus d'autres pièces sur un échiquier.

    Args:
        couleur (str): La couleur avec laquelle créer la pièce.
        peut_sauter (bool): La valeur avec laquelle l'attribut peut_sauter doit être initialisé.

    """
    def __init__(self, couleur, peut_sauter):
        # Validation si la couleur reçue est valide.
        assert couleur in ('B', 'N')

        # Création des attributs avec les valeurs reçues.
        self.couleur = couleur
        self.peut_sauter = peut_sauter

    def est_blanc(self):
        """Retourne si oui ou non la pièce est blanche.

        Returns:
            bool: True si la pièce est blanche, et False autrement.

        """
        if self.couleur == 'B':
            self.couleur = 'blanc'

        return self.couleur

    def est_noir(self):
        """Retourne si oui ou non la pièce est noire.

        Returns:
            bool: True si la pièce est noire, et False autrement.

        """
        if self.couleur == 'N':
            self.couleur = 'noir'

        return self.couleur

    def peut_se_deplacer_vers(self, position_source, position_cible):
        """Vérifie si, selon les règles du jeu d'échecs, la pièce peut se déplacer d'une position à une autre.

        Une position est une chaîne de deux caractères.
            Le premier caractère est une lettre entre a et h, représentant la colonne de l'échiquier.
            Le second caractère est un chiffre entre 1 et 8, représentant la rangée de l'échiquier.

        Args:
            position_source (str): La position source, suivant le format ci-haut. Par exemple, 'a8', 'f3', etc.
            position_cible (str): La position cible, suivant le format ci-haut. Par exemple, 'b6', 'h1', etc.

        Warning:
            Comme nous sommes dans la classe de base et non dans l'une des classes filles, nous ne savons pas
            (encore) comment cette pièce se déplace. Cette méthode est donc à redéfinir dans chacune des
            classes filles.

        Warning:
            Comme la classe Piece est indépendante de l'échiquier (et donc on ne sait pas si une pièce est "dans le
            chemin"), on doit ignorer le contenu de l'échiquier : on ne se concentre que sur les règles de mouvement
            des pièces.

        Returns:
            bool: True si le déplacement est valide en suivant les règles de la pièce, et False autrement.

        """
        # On lance une exception (on y reviendra) indiquant que ce code n'a pas été implémenté. Ne touchez pas
        # à cette méthode : réimplémentez-la dans les classes filles!
        raise NotImplementedError

    def peut_faire_une_prise_vers(self, position_source, position_cible):
        """Vérifie si, selon les règles du jeu d'échecs, la pièce peut "manger" (faire une prise) une pièce ennemie.
        Pour la plupart des pièces, la règle est la même, on appelle donc la méthode peut_se_deplacer_vers.

        Si ce n'est pas le cas pour une certaine pièce, on peut simplement redéfinir cette méthode pour programmer
        la règle.

        Args:
            position_source (str): La position source, suivant le format ci-haut. Par exemple, 'a8', 'f3', etc.
            position_cible (str): La position cible, suivant le format ci-haut. Par exemple, 'b6', 'h1', etc.

        Returns:
            bool: True si la prise est valide en suivant les règles de la pièce, et False autrement.

        """
        return self.peut_se_deplacer_vers(position_source, position_cible)

    def intafy(self, pos_s, pos_c):
        """Transform les valeurs de positions en int

            Une position est une chaîne de deux caractères.
                Le premier caractère est une lettre entre a et h, représentant la colonne de l'échiquier.
                Le second caractère est un chiffre entre 1 et 8, représentant la rangée de l'échiquier.

            Args:
                position_source (str): La position source, suivant le format ci-haut. Par exemple, 'a8', 'f3', etc.
                position_cible (str): La position cible, suivant le format ci-haut. Par exemple, 'b6', 'h1', etc.

            Returns:
                list: Retourne une list de nombre pour les deux positions afin de facilité la comparaison

            """
        ma_list = []
        for word in pos_s:  # On divise les lettres des nombres puis on les mets dans une liste
            ma_list += [word]
        for word in pos_c:
            ma_list += [word]
        numS = int(ma_list[1])  # On ne change pas les nombres on fait simplement les garder pour plus tard
        numC = int(ma_list[3])
        letrS = ''
        letrC = ''
        value = 9
        while type(letrS) == type('') or type(letrC) == type(''):  # On boucle tant que les lettres sont des strngs et non des integer
            if value == 9: # Si la variable value est égal à neuf on entre ici
                if ma_list[0] == ma_list[2] and ma_list[0] == 'a':  # Si le string des deux lettres est équivalent à a on les transforms en int 9
                    letrS = int(value)
                    letrC = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[0] == 'a':# Si le string de la lettre source est égal à a on le transform en neuf à l'intérieur de la variable lettre source
                    letrS = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[2] == 'a':  # Même concept mais pour la lettre cible
                    letrC = int(value)
            if value == 10:  # Pour lui et les suivant c'est le même concept qu'avec a
                if ma_list[0] == ma_list[2] and ma_list[0] == 'b':
                    letrS = int(value)
                    letrC = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[0] == 'b':
                    letrS = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[2] == 'b':
                    letrC = int(value)
            if value == 11:
                if ma_list[0] == ma_list[2] and ma_list[0] == 'c':
                    letrS = int(value)
                    letrC = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[0] == 'c':
                    letrS = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[2] == 'c':
                    letrC = int(value)
            if value == 12:
                if ma_list[0] == ma_list[2] and ma_list[0] == 'd':
                    letrS = int(value)
                    letrC = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[0] == 'd':
                    letrS = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[2] == 'd':
                    letrC = int(value)
            if value == 13:
                if ma_list[0] == ma_list[2] and ma_list[0] == 'e':
                    letrS = int(value)
                    letrC = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[0] == 'e':
                    letrS = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[2] == 'e':
                    letrC = int(value)
            if value == 14:
                if ma_list[0] == ma_list[2] and ma_list[0] == 'f':
                    letrS = int(value)
                    letrC = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[0] == 'f':
                    letrS = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[2] == 'f':
                    letrC = int(value)
            if value == 15:
                if ma_list[0] == ma_list[2] and ma_list[0] == 'g':
                    letrS = int(value)
                    letrC = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[0] == 'g':
                    letrS = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[2] == 'g':
                    letrC = int(value)
            if value == 16:
                if ma_list[0] == ma_list[2] and ma_list[0] == 'h':
                    letrS = int(value)
                    letrC = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[0] == 'h':
                    letrS = int(value)
                elif ma_list[0] != ma_list[2] and ma_list[2] == 'h':
                    letrC = int(value)
            value += 1
        listofints = [letrS, numS, letrC, numC] # On créer une liste des valeurs en int final pour les retourner

        return listofints


class Pion(Piece):

    def __init__(self, couleur):
        super().__init__(couleur, False)

    def peut_se_deplacer_vers(self, position_source, position_cible):

        if self.couleur == 'B':
            comparateur = self.intafy(position_source, position_cible)

            if comparateur[1] == 2 and comparateur[3] - comparateur[1] == 2 and comparateur[2]-comparateur[0] == 0:# Si le pion blanc est à ça position initiale il pourra se déplacer de deux
                return True

            elif comparateur[1] >= 2 and comparateur[3]-comparateur[1] == 1 and comparateur[2]-comparateur[0] == 0: # Si le pion blanc est à n'importe qu'elle autres position que ça position initiale il pourra se déplacer de un
                return True

        elif self.couleur == 'N':
            comparateur2 = self.intafy(position_source, position_cible)

            if comparateur2[1] == 7 and comparateur2[3] - comparateur2[1] == -2 and comparateur2[2]-comparateur2[0] == 0:# Même concept que le pion blanc mais avec des valeurs inverser
                return True
            elif comparateur2[1] <= 7 and comparateur2[3] - comparateur2[1] == -1 and comparateur2[2]-comparateur2[0] == 0:
                return True
        return False

    def peut_faire_une_prise_vers(self, position_source, position_cible):
        comparateur = self.intafy(position_source, position_cible)
        if self.couleur == 'B':
            if comparateur[2]-comparateur[0] == 1 and comparateur[3] - comparateur[1] == 1 or comparateur[2]-comparateur[0] == -1 and comparateur[3] - comparateur[1] == 1: # Le pion peut faire une prise en diagonal
                return True
        elif self.couleur == 'N':
            comparateur2 = self.intafy(position_source, position_cible)
            if comparateur2[2] - comparateur2[0] == 1 and comparateur2[3] - comparateur2[1] == -1 or comparateur2[2] - comparateur2[0] == -1 and comparateur2[3] - comparateur2[1] == -1: # Même chose que le pion blanc
                return True
        return False

    def __repr__(self):
            """Redéfinit comment on affiche un pion à l'écran. Nous utilisons la constante UTILISER_UNICODE
            pour déterminer comment afficher le pion.

            Returns:
                str: La chaîne de caractères représentant le pion.

            """
            if self.couleur == 'B':
                if UTILISER_UNICODE:
                    return '\u2659'
                else:
                    return 'PB'
            else:
                if UTILISER_UNICODE:
                    return '\u265f'
                else:
                    return 'PN'


class Tour(Piece):

    def __init__(self, couleur):
        super().__init__(couleur, False)

    def peut_se_deplacer_vers(self, position_source, position_cible):


        comparateur = self.intafy(position_source, position_cible)

        if comparateur[2] == comparateur[0] and comparateur[3] != comparateur[1]: # Si la colonne source est égal la colonne cible et que les rangées ne s'équivalent pas, la tour peut se déplacer à l'horizontale
            return True
        elif comparateur[3] == comparateur[1] and comparateur[2] != comparateur[0]:# Si la rangée source est égal la rangée cible et que les colonnes ne s'équivalent pas, la tour peut se déplacer à la verticale
            return True
        return False

    def __repr__(self):
        if self.couleur == 'B':
            if UTILISER_UNICODE:
                return '\u2656'
            else:
                return 'TB'
        else:
            if UTILISER_UNICODE:
                return '\u265c'
            else:
                return 'TN'

class Cavalier(Piece):

    def __init__(self, couleur):
        super().__init__(couleur, True)

    def peut_se_deplacer_vers(self, position_source, position_cible):

        comparateur = self.intafy(position_source, position_cible)

        if abs(comparateur[0] - comparateur[2]) == 1 and abs(comparateur[1] - comparateur[3]) == 2:  # s'il se déplace d'une colonne et de deux rangées
            return True                                                                              # le déplacemant est valide
        elif abs(comparateur[0] - comparateur[2]) == 2 and abs(comparateur[1] - comparateur[3]) == 1:  # s'il se déplace de deux colonnes et d'une rangée
            return True                                                                                # le déplacemant est valide
        return False

    def __repr__(self):
        if self.couleur == 'B':
            if UTILISER_UNICODE:
                return '\u2658'
            else:
                return 'CB'
        else:
            if UTILISER_UNICODE:
                return '\u265e'
            else:
                return 'CN'


class Fou(Piece):

    def __init__(self, couleur):
        super().__init__(couleur, False)

    def peut_se_deplacer_vers(self, position_source, position_cible):

        comparateur = self.intafy(position_source, position_cible)

        dif = abs(comparateur[2] - comparateur[0]) # La dfférence de valeur entre les colonnes
        if comparateur[1] > comparateur[3]: # Si la valeur de rangée source est plus grande que la valeur cible on met la différence négative
            dif = -dif

        if comparateur[2] != comparateur[0] and dif + comparateur[1] == comparateur[3]: # Si les colonnes ne sont pas égal et que la différence entre les colonnes + la valeur source est égal à la valeur cible, le fou se déplace en diagonal
                return True
        return False

    def __repr__(self):
        if self.couleur == 'B':
            if UTILISER_UNICODE:
                return '\u2657'
            else:
                return 'FB'
        else:
            if UTILISER_UNICODE:
                return '\u265d'
            else:
                return 'FN'


class Roi(Piece):

    def __init__(self, couleur):
        super().__init__(couleur, False)

    def peut_se_deplacer_vers(self, position_source, position_cible):
        comparateur = self.intafy(position_source, position_cible)
        if comparateur[2] - comparateur[0] == 0 and comparateur[3] - comparateur[1] == 1 or comparateur[3] - comparateur[1] == -1: # Même déplacement que le pion
            return True
        elif comparateur[3] - comparateur[1] == 0 and comparateur[2] - comparateur[0] == 1 or comparateur[2] - comparateur[0] == -1:
            return True
        elif comparateur[2] != comparateur[0] and comparateur[2] - comparateur[0] == 1:
            return True
        return False

    def __repr__(self):
        if self.couleur == 'B':
            if UTILISER_UNICODE:
                return '\u2654'
            else:
                return 'RB'
        else:
            if UTILISER_UNICODE:
                return '\u265a'
            else:
                return 'RN'

class Dame(Piece):

    def __init__(self, couleur):
        super().__init__(couleur, False)

    def peut_se_deplacer_vers(self, position_source, position_cible):

        comparateur = self.intafy(position_source, position_cible)

        if comparateur[3] == comparateur[1] and comparateur[2] != comparateur[0]: # Même déplacement que la tour
            return True
        elif comparateur[2] == comparateur[0] and comparateur[3] != comparateur[1]:
            return True
        dif = abs(comparateur[2] - comparateur[0])# Même déplacement que le fou
        if comparateur[1] > comparateur[3]:
            dif = -dif

        if comparateur[2] != comparateur[0] and dif + comparateur[1] == comparateur[3]:
                return True
        return False

    def __repr__(self):
        if self.couleur == 'B':
            if UTILISER_UNICODE:
                return '\u2655'
            else:
                return 'DB'
        else:
            if UTILISER_UNICODE:
                return '\u265b'
            else:
                return 'DN'
